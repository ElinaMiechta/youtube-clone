import {useState } from 'react'
import {BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import Header from "./components/Header";
import "./styles/themes/default/themes.scss";
import MenuBlock from "./components/Menu/MenuBlock";
import MenuProvider from "./components/utils/menuContext";
import { DarkModeProvider } from "./components/utils/darkMode";
import TransitionsModal from './components/ModalDialog'
import Home from './components/pages/Home'
import TestYTUser from './components/pages/TestYTUser'
function App() {
  const [open, setOpen] = useState(true);


  return (
    <Router>
    
    <DarkModeProvider>
    <MenuProvider>
        <div className="mainWrapper">
          <TransitionsModal handleClose={() => setOpen(false)} open={open}/>
          <Header />
          <div className="mainWrapper--mainSection">
      <MenuBlock />
          <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/user" component={TestYTUser}/>
          </Switch>
          </div>
        </div>
     
    </MenuProvider>
    </DarkModeProvider>
    </Router>
  );
}
export default App;

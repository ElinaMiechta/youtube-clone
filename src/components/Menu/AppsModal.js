import YouTubeIcon from "@material-ui/icons/YouTube";
import { useDarkMode } from "../utils/darkMode";
import { Styles } from "../Styles";

const AppsModal = () => {
  const { darkMode } = useDarkMode();
  return (
    <>
      <Styles darkMode={darkMode} />
      <div
        className="appContainer"
        style={{
          backgroundColor: `${darkMode ? "rgb(32, 32, 32)" : "#fff"}`,
          borderColor: `${darkMode ? "#333333" : "#cccccc"}`,
        }}>
        <ul className="appContainer--list">
          <li className="appContainer__listItem outline">
            <img
              src="/assets/images/yt_tv.png"
              className="listItem__ytIcon"
              alt="YoutTube Apps"
            />
            <span style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
              YouTube TV
            </span>
          </li>
          <li className="appContainer__listItem">
            <img
              src="/assets/images/yt_music.png"
              className="listItem__ytIcon"
              alt="YoutTube Apps"
            />
            <span style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
              YouTube Music
            </span>
          </li>
          <li className="appContainer__listItem outline">
            <img
              src="/assets/images/yt_kids.png"
              className="listItem__ytIcon"
              alt="YoutTube Apps"
            />
            <span style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
              {" "}
              Youtube Kids
            </span>
          </li>
          <li className="appContainer__listItem">
            <YouTubeIcon className="listItem__ytIcon" />
            <span style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
              Akademia Tworcow
            </span>
          </li>
          <li className="appContainer__listItem">
            <YouTubeIcon className="listItem__ytIcon" />
            <span style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
              YouTube dla wykonawcow
            </span>
          </li>
        </ul>
      </div>
    </>
  );
};

export default AppsModal;

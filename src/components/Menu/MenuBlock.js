import { useRef } from "react";
import { useMenu } from "../utils/menuContext";
import SidebarWide from "../SideBar/SidebarWide";
import SidebarNarrow from "../SideBar/SidebarNarrow";
import { useDarkMode } from "../utils/darkMode";

const MenuBlock = () => {
  const options = useRef(null);
  const { wide } = useMenu();
  const { darkMode } = useDarkMode();

  const activeToggle = (e) => {
    const childOpts = options.current.childNodes;
    childOpts.forEach((el) => {
      console.log(e.target, el);
      return el === e.target
        ? el.classList.add("active")
        : el.classList.remove("active");
    });
  };

  return (
    <>
      {wide ? (
        <SidebarWide
          listRef={options}
          activeToggle={activeToggle}
          darkMode={darkMode}
        />
      ) : (
        <SidebarNarrow
          listRef={options}
          activeToggle={activeToggle}
          darkMode={darkMode}
        />
      )}
    </>
  );
};

export default MenuBlock;

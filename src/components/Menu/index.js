import { useEffect } from "react";
import WbSunnyIcon from "@material-ui/icons/WbSunny";
import Brightness2Icon from "@material-ui/icons/Brightness2";
import TranslateIcon from "@material-ui/icons/Translate";
import LanguageIcon from "@material-ui/icons/Language";
import SettingsIcon from "@material-ui/icons/Settings";
import SecurityIcon from "@material-ui/icons/Security";
import HelpIcon from "@material-ui/icons/Help";
import SmsFailedIcon from "@material-ui/icons/SmsFailed";
import { useDarkMode } from "../utils/darkMode";
import { Styles } from "../Styles";

const MenuModal = () => {
  const { darkMode, setTheme } = useDarkMode();

  useEffect(() => {
    localStorage.setItem("theme", darkMode ? "night" : "day");
  }, [darkMode]);

  return (
    <>
      <Styles darkMode={darkMode} />
      <div
        className="menuContainer"
        style={{
          backgroundColor: `${darkMode ? "rgb(32, 32, 32)" : "#fff"}`,
          borderColor: `${darkMode ? "#333333" : "#cccccc"}`,
        }}>
        <ul className="menuContainer--list">
          <li className="menuContainer__listItem">
            {darkMode ? (
              <WbSunnyIcon className="listItem__icon" onClick={setTheme} />
            ) : (
              <Brightness2Icon className="listItem__icon" onClick={setTheme} />
            )}
            <span
              onClick={setTheme}
              style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
              Wyglad jasny/ciemny
            </span>
          </li>
          <li className="menuContainer__listItem">
            <TranslateIcon className="listItem__icon" />
            <span style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
              Jezyk: polski
            </span>
          </li>
          <li className="menuContainer__listItem">
            <LanguageIcon className="listItem__icon" />
            <span style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
              {" "}
              Lokalizacja: Polska
            </span>
          </li>
          <li className="menuContainer__listItem">
            <SettingsIcon className="listItem__icon" />
            <span style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
              Ustawienia
            </span>
          </li>
          <li className="menuContainer__listItem">
            <SecurityIcon className="listItem__icon" />
            <span style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
              Twoje dane w YouTube
            </span>
          </li>
          <li className="menuContainer__listItem">
            <HelpIcon className="listItem__icon" />
            <span style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
              Pomoc
            </span>
          </li>
          <li className="menuContainer__listItem">
            <SmsFailedIcon className="listItem__icon" />
            <span style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
              {" "}
              Przeslij opinie
            </span>
          </li>
        </ul>
      </div>
    </>
  );
};

export default MenuModal;

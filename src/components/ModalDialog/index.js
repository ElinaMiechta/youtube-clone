import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: "#181818",
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    width: "450px",
    height: "300px",
    position: "relative",
  },
}));

export default function TransitionsModal({ open, handleClose }) {
  const classes = useStyles();

  return (
    <>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}>
        <Fade in={open}>
          <div className={classes.paper}>
            <span className="modal__close" onClick={handleClose}>
              X
            </span>
            <h2 className="modal__header" id="transition-modal-title">
              README{" "}
            </h2>
            <p className="modal__text" id="transition-modal-description">
              To jest wersja <span>YouTube</span> dla niezalogowanego użytkowanika, posiada
              Router, który przekierowuje na strone usera (po klinkięciu w dowolną nazwe kanału obok ikonki Avataru),
               ma klikalne MenuBars w lewym górnym rogu, które rozwijają schowane boczne menu, a także ikonki
              w prawym górnym rogu strony, które pokazują dwa schowane menu. Po klinknięciu w "Wygląd
              jasny/ciemny" mają się zmienić kolory na stronie.
            </p>
          </div>
        </Fade>
      </Modal>
    </>
  );
}

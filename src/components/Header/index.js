import MenuIcon from "@material-ui/icons/Menu";
import SearchIcon from "@material-ui/icons/Search";
import MicIcon from "@material-ui/icons/Mic";
import AppsIcon from "@material-ui/icons/Apps";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import { Button } from "@material-ui/core";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import MenuModal from "../Menu";
import AppsModal from "../Menu/AppsModal";
import { useModal } from "../utils/useModal";
import { useMenu } from "../utils/menuContext";
import { useDarkMode } from "../utils/darkMode";
import { Link } from "react-router-dom";

const Header = () => {
  const { showMenu, showApps, handleShowMenu, handleShowApps } = useModal();
  const { displayMenu } = useMenu();
  const { darkMode } = useDarkMode();

  return (
    <header
      className="header"
      style={{ backgroundColor: `${darkMode ? "rgb(32, 32, 32)" : "#fff"}` }}>
      <div className="header__col1">
        <MenuIcon
          className="header__menuIcon"
          onClick={displayMenu}
          style={{ color: `${darkMode ? "#cccccc" : "inherit"}` }}
        />
        {darkMode ? (
          <>
            <Link to="/">
              {" "}
              <img
                className="header__ytLogo"
                src="/assets/images/yt_darkLogo.png"
                alt="Youtube"
              />
            </Link>
          </>
        ) : (
          <>
            <Link to="/">
              <img
                className="header__ytLogo"
                src="/assets/images/yt_logo.jpeg"
                alt="Youtube"
              />
            </Link>
          </>
        )}
      </div>
      <div className="header__col2">
        <div
          className="header__col2--wrapper"
          style={{
            borderColor: `${darkMode ? "#313131" : "#cccccc"}`,
            boxShadow: `${darkMode ? "none" : "0 1px 2px #F2F2F2"}`,
          }}>
          <input
            type="text"
            placeholder="Szukaj"
            style={{
              backgroundColor: `${darkMode ? "#121212" : "inherit"}`,
              color: `${darkMode ? "#fff" : "inherit"}`,
            }}
          />
          <div
            className="col2--wrapper__search"
            style={{
              backgroundColor: `${darkMode ? "#313131" : "#f0f0f0"}`,
              borderLeft: `${darkMode ? "#121212" : "inherit"}`,
            }}>
            <SearchIcon className="header__col2--icon" />
          </div>
        </div>
      </div>
      <div className="header__col3">
        <MicIcon
          className="header__micro ytIcon"
          style={{ color: `${darkMode ? "#cccccc" : "inherit"}` }}
        />
        <div className="header__col3--menuOpt">
          <AppsIcon
            className="header__app ytIcon"
            onClick={handleShowApps}
            style={{ color: `${darkMode ? "#cccccc" : "inherit"}` }}
          />
          <MoreVertIcon
            className="header__dots ytIcon"
            onClick={handleShowMenu}
            style={{ color: `${darkMode ? "#cccccc" : "inherit"}` }}
          />
        </div>
        <Button className="header__col3--btn ytIcon" variant="outlined">
          <AccountCircleIcon className="col3--btn__icon " />
          Zaloguj się
        </Button>
        {showMenu && <MenuModal />}
        {showApps && <AppsModal />}
      </div>
    </header>
  );
};

export default Header;

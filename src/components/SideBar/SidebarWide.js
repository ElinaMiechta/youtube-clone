import HomeIcon from "@material-ui/icons/Home";
import ExploreIcon from "@material-ui/icons/Explore";
import SubscriptionsIcon from "@material-ui/icons/Subscriptions";
import { FaYoutubeSquare } from "react-icons/fa";
import HistoryIcon from "@material-ui/icons/History";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { Button } from "@material-ui/core";
import YouTubeIcon from "@material-ui/icons/YouTube";
import WifiTetheringIcon from "@material-ui/icons/WifiTethering";
import SettingsIcon from "@material-ui/icons/Settings";
import HelpIcon from "@material-ui/icons/Help";
import SmsFailedIcon from "@material-ui/icons/SmsFailed";

const SidebarWide = ({listRef, activeToggle, darkMode}) => {
      return (
            <div className="sideMenu" style={{backgroundColor: `${darkMode ? 'rgb(32, 32, 32)' : '#fff'}`}}>
            <div className="sideMenu__menuOpt">
              <ul className="sideMenu__menuOpt--list" ref={listRef}>
                <li className="active" onClick={activeToggle}>
                  <HomeIcon className="menuOpt__icon active" />
                  <span style={{color: `${darkMode ? '#fff' : 'inherit'}`}}>Główna</span>
                </li>
                <li onClick={activeToggle}>
                  <ExploreIcon className="menuOpt__icon" />
                  <span style={{color: `${darkMode ? '#fff' : 'inherit'}`}}>Odkrywaj</span>
                </li>
                <li className="outline" onClick={activeToggle}>
                  <SubscriptionsIcon className="menuOpt__icon" />
                  <span style={{color: `${darkMode ? '#fff' : 'inherit'}`}}>Subskrybuj</span>
                </li>
                <li onClick={activeToggle}>
                  <FaYoutubeSquare className="menuOpt__icon" />
                  <span style={{color: `${darkMode ? '#fff' : 'inherit'}`}}>Biblioteka</span>
                </li>
                <li className="outline" onClick={activeToggle}>
                  <HistoryIcon className="menuOpt__icon" />
                  <span style={{color: `${darkMode ? '#fff' : 'inherit'}`}}>Historia</span>
                </li>
              </ul>
            </div>
      
            <div className="sideMenu__info outline">
              <p style={{color: `${darkMode ? '#fff' : 'inherit'}`}}>
                Zaloguj się, by polubić film, zostawić komentarz lub zasubskrybować
                kanał.
              </p>
              <Button className="ytButton" variant="outlined">
                <AccountCircleIcon className="col3--btn__icon accountIcon " />
                Zaloguj się
              </Button>
            </div>
      
            <div className="sideMenu__more">
              <span style={{color: `${darkMode ? '#cccccc' : 'inherit'}`}}>WIECEJ Z YOUTUBE</span>
              <ul className="sideMenu__menuOpt--list">
                <li>
                  <YouTubeIcon className="menuOpt__icon" />
                  <span style={{color: `${darkMode ? '#fff' : 'inherit'}`}}>YouTube Premium</span>
                </li>
                <li className="outline">
                  <WifiTetheringIcon className="menuOpt__icon" />
                  <span style={{color: `${darkMode ? '#fff' : 'inherit'}`}}>Na żywo</span>
                </li>
                <li>
                  <SettingsIcon className="menuOpt__icon" />
                  <span style={{color: `${darkMode ? '#fff' : 'inherit'}`}}>Ustawienia</span>
                </li>
                <li>
                  <HelpIcon className="menuOpt__icon" />
                  <span style={{color: `${darkMode ? '#fff' : 'inherit'}`}}>Pomoc</span>
                </li>
                <li>
                  <SmsFailedIcon className="menuOpt__icon" />
                  <span style={{color: `${darkMode ? '#fff' : 'inherit'}`}}>Przeslij opinie</span>
                </li>
              </ul>
            </div>
      
            <div className="sideMenu__appInfo">
              <span style={{color: `${darkMode ? '#cccccc' : 'inherit'}`}}>Informacje</span>
              <span style={{color: `${darkMode ? '#cccccc' : 'inherit'}`}}>Centrum Prasowe</span>
              <span style={{color: `${darkMode ? '#cccccc' : 'inherit'}`}}>Prawa autorskie</span>
              <span style={{color: `${darkMode ? '#cccccc' : 'inherit'}`}}>Skontaktuj sie z nami</span>
              <span style={{color: `${darkMode ? '#cccccc' : 'inherit'}`}}>Twórcy</span>
              <span style={{color: `${darkMode ? '#cccccc' : 'inherit'}`}}>Reklamy</span>
              <span style={{color: `${darkMode ? '#cccccc' : 'inherit'}`}}>Deweloperzy</span>
            </div>
            <span id="google"> &copy; 2021 YouTube :)</span>
          </div>
      )
}

export default SidebarWide

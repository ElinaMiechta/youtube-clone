import HomeIcon from "@material-ui/icons/Home";
import ExploreIcon from "@material-ui/icons/Explore";
import SubscriptionsIcon from "@material-ui/icons/Subscriptions";
import { FaYoutubeSquare } from "react-icons/fa";
import HistoryIcon from "@material-ui/icons/History";

const SidebarNarrow = ({ activeToggle, listRef, darkMode }) => {
  return (
    <div
      className="sideMenuNarrow"
      style={{ backgroundColor: `${darkMode ? "rgb(32, 32, 32)" : "#fff"}` }}>
      <div className="sideMenu__menuOpt">
        <ul className="sideMenu__menuOpt--list narrow" ref={listRef}>
          <li
            className="active"
            style={{
              backgroundColor: `${darkMode ? "rgb(32, 32, 32)" : "inherit"}`,
            }}>
            <HomeIcon className="menuOpt__icon active" />
            <span style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
              Główna
            </span>
          </li>
          <li>
            <ExploreIcon className="menuOpt__icon" onClick={activeToggle} />
            <span>Odkrywaj</span>
          </li>
          <li>
            <SubscriptionsIcon
              className="menuOpt__icon"
              onClick={activeToggle}
            />
            <span>Subskrybuj</span>
          </li>
          <li>
            <FaYoutubeSquare className="menuOpt__icon" onClick={activeToggle} />
            <span>Biblioteka</span>
          </li>
          <li>
            <HistoryIcon className="menuOpt__icon" onClick={activeToggle} />
            <span>Historia</span>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default SidebarNarrow;

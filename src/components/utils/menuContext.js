import React, { useState, useContext } from "react";

const MenuContext = React.createContext(true);

export const useMenu = () => useContext(MenuContext);

export const MenuProvider = ({ children }) => {
  const [showWide, setShowWide] = useState(false);

  const displayMenu = () => {
    setShowWide(!showWide);
  };

  return (
    <MenuContext.Provider
      value={{
        wide: showWide,
        displayMenu,
      }}>
      {children}
    </MenuContext.Provider>
  );
};

export default MenuProvider;

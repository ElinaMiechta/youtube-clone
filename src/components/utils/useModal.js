import { useState } from "react";

export const useModal = () => {
  const [showMenu, setShowMenu] = useState(false);
  const [showApps, setShowApps] = useState(false);

  const handleShowMenu = () => {
    setShowMenu(!showMenu);
    if (showApps) {
      setShowApps(false);
    }
  };
  const handleShowApps = () => {
    setShowApps(!showApps);
    if (showMenu) {
      setShowMenu(false);
    }
  };

  return { showMenu, showApps, handleShowMenu, handleShowApps };
};

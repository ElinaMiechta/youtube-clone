import Card from "./Card";
import { videoData } from "../data/videoData";
import { popularVideoData } from "../data/videoData";
import { useMenu } from "../utils/menuContext";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import {useDarkMode} from '../utils/darkMode';

const VideoSection = () => {
  const { wide } = useMenu();
  const { darkMode } = useDarkMode();
  return (
    <div
      className="videoSection videoWrapper"
      style={{
        flexBasis: `${wide ? "90%" : "100%"}`,
        backgroundColor: `${darkMode ? "#181818" : "#f0f0f0"}`,
      }}>
      {videoData.map((video, index) => (
        <Card
          key={index}
          imgSrc={video.imgSrc}
          title={video.title}
          channelName={video.channelName}
          views={video.views}
          channelUser={video.channelUser}
          date={video.date}
          time={video.time}
          darkMode={darkMode}
        />
      ))}

      <div
        className="videoSection__divider"
        style={{
          backgroundColor: `${darkMode ? "#333333" : "#cccccc"}`,
        }}></div>
      <div className="videoSection__popular">
        <h2 style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>Na czasie</h2>
        <div
          className="videoWrapper"
          style={{ backgroundColor: `${darkMode ? "#181818" : "#f0f0f0"}` }}>
          {popularVideoData.map((video, index) => (
            <Card
              key={video.title + index}
              imgSrc={video.imgSrc}
              title={video.title}
              channelName={video.channelName}
              views={video.views}
              channelUser={video.channelUser}
              date={video.date}
              time={video.time}
              darkMode={darkMode}
            />
          ))}
          <div className="videoSection__popular--more">
            <ExpandMoreIcon />
          </div>
        </div>
        <div
          className="videoSection__divider"
          style={{
            backgroundColor: `${darkMode ? "#333333" : "#cccccc"}`,
          }}></div>
      </div>

      <div className="videoSection__popular rest">
        {videoData.map((video, index) => (
          <Card
            key={index}
            imgSrc={video.imgSrc}
            title={video.title}
            channelName={video.channelName}
            views={video.views}
            channelUser={video.channelUser}
            date={video.date}
            time={video.time}
            darkMode={darkMode}
          />
        ))}
      </div>
    </div>
  );
};

export default VideoSection;

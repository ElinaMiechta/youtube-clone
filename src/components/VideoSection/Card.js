import { Avatar } from "@material-ui/core";
import ScheduleIcon from "@material-ui/icons/Schedule";
import PlaylistAddIcon from "@material-ui/icons/PlaylistAdd";
import { useMenu } from "../utils/menuContext";
import {Link} from 'react-router-dom'

const Card = ({
  imgSrc,
  title,
  channelName,
  channelUser,
  views,
  date,
  time,
  darkMode
}) => {
  const { wide } = useMenu();
  const saveAttr = () => {
    localStorage.setItem('user', channelName);
    localStorage.setItem('userAvatar', channelUser);
    localStorage.setItem('title', title);
    localStorage.setItem('views',views);
    localStorage.setItem('imgSrc', imgSrc);
    localStorage.setItem('date', date);
  }
  return (
    <div
      className="videoCard"
      style={{ width: `${!wide ? "320px" : "264px"}` }}>
      <div className="videoCard__video">
        <img
          src={imgSrc}
          alt="yt video"
          style={{ width: `${!wide ? "100%" : "264px"}` }}
        />
        <div className="videoCard__overlay">
          <ScheduleIcon />
          <PlaylistAddIcon />
        </div>
        <div className="videoCard__timing">
          <span>{time}</span>
        </div>
      </div>

      <div className="videoCard__infoBlock">
        <div className="videoCard__videoTitle">
          <Avatar
            className="videoCard__user"
            alt={channelName}
            src={channelUser}
          />
          <span style={{color: `${darkMode ? '#fff' : 'inherit'}`}}>{title}</span>
        </div>
        <div className="videoCard__info">
          <p onClick={saveAttr}><Link to="/user">{channelName}</Link></p>
          <p>
            {views} &middot; {date}
          </p>
        </div>
      </div>
    </div>
  );
};

export default Card;

const SingleVideo = ({ darkMode }) => {
  const image = localStorage.getItem("imgSrc"),
    title = localStorage.getItem("title"),
    views = localStorage.getItem("views"),
    date = localStorage.getItem("date");

  return (
    <div className="video">
      <div className="video__left">
        <p>{title}</p>
        <img src={image} alt="Featured Video From User" />
      </div>
      <div className="video__right">
        <p
          className="video__right--title"
          style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
          {title}
        </p>
        <p className="video__right--views">
          {views} &middot; {date}
        </p>
        <p
          className="video__right--text"
          style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur.
        </p>
        <p className="video__right--link">Pokaż więcej</p>
      </div>
    </div>
  );
};

export default SingleVideo;

export const videoData = [
  {
    title: "Classical Music for Reading - Mozart, Chopin...",
    imgSrc:
      "https://images.unsplash.com/photo-1507838153414-b4b713384a76?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    channelName: "JALIDONMUSIC",
    views: "41 mln wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1541701494587-cb58502866ab?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    date: "2 lata temu",
    time: "1:39:18",
  },
  {
    title: "Muzyka Klasyczna do Nauki | Spokojna Muzyka...",
    imgSrc:
      "https://images.unsplash.com/photo-1510915361894-db8b60106cb1?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    channelName: "Just Music",
    views: "111tys. wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1567095761054-7a02e69e5c43?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    date: "7 dni temu",
    time: "2:30:21",
  },
  {
    title: "30 Most Beautiful Classical Pieces",
    imgSrc:
      "https://images.unsplash.com/photo-1441974231531-c6227db76b6e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1051&q=80",
    channelName: "JALIDONMUSIC",
    views: "2.1 mln wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1541701494587-cb58502866ab?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    date: "4 miesiące temu",
    time: "1:21:00",
  },
  {
    title: "Music for Relax",
    imgSrc:
      "https://images.unsplash.com/photo-1470071459604-3b5ec3a7fe05?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1140&q=80",
    channelName: "OuterSpace",
    views: "56 tys. wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1524169358666-79f22534bc6e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    date: "1 dzień temu",
    time: "49:14",
  },
  {
    title: "Classical Music - Romantic Age",
    imgSrc:
      "https://images.unsplash.com/photo-1505150099521-fde7970bcc3a?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    channelName: "Classical Chillout",
    views: "1.1 mln wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1531169509526-f8f1fdaa4a67?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    date: "7 dni temu",
    time: "22:29:13",
  },
  {
    title: "Music for work",
    imgSrc:
      "https://images.unsplash.com/photo-1497032628192-86f99bcd76bc?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    channelName: "Classical Chillout",
    views: "600 tys. wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1531169509526-f8f1fdaa4a67?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    date: "2 miesące temu",
    time: "2:09:13",
  },
  {
    title: "Chopin Nocturne op. 9 No.2",
    imgSrc:
      "https://images.unsplash.com/photo-1514680000430-93a1864bd79d?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1024&q=80",
    channelName: "andrea suezuess",
    views: "123 mln wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1557862921-37829c790f19?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1051&q=80",
    date: "12 dni temu",
    time: "1:28:19",
  },
  {
    title: "Relaing Jazz Piano Radio - Slow Jazz...",
    imgSrc:
      "https://images.unsplash.com/photo-1415201364774-f6f0bb35f28f?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    channelName: "Jazz Lovers",
    views: "10.2 mln wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1580832945253-9a8f87b606f2?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    date: "1 rok temu",
    time: "55:00",
  },
];

export const popularVideoData = [
  {
    title: "React Js for Complete Beginners | Full Course",
    imgSrc: "https://ru.reactjs.org/logo-og.png",
    channelName: "Jan Kowalski",
    views: "1.1 mln wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1587620962725-abab7fe55159?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1189&q=80",
    date: "18 dni temu",
    time: "3:59:23",
  },
  {
    title: "React Hooks Tutorial",
    imgSrc: "https://www.zzz.com.ua/blog/wp-content/uploads/maxresdefault.jpg",
    channelName: "Ben Amo",
    views: "650 tys. wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1568602471122-7832951cc4c5?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    date: "3 dni temu",
    time: "2:09:13",
  },
  {
    title: "Top 3 Programming Languages You Need To Check...",
    imgSrc:
      "https://images.unsplash.com/photo-1542831371-29b0f74f9713?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    channelName: "Lets Do IT!",
    views: "123 tys. wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1571171637578-41bc2dd41cd2?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    date: "1 dzień temu",
    time: "48:28:19",
  },
  {
    title: "TDD in JavaScript",
    imgSrc: "https://miro.medium.com/max/4800/1*aWNHVJ1_YmYjlRsQTftPTw.png",
    channelName: "Ben Amo",
    views: "111 tys. wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1568602471122-7832951cc4c5?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    date: "2 dni temu",
    time: "01:00:29",
  },
];

export const userYtPopularData = [
  {
    title: "Classical Music for Reading - Mozart, Chopin...",
    imgSrc:
      "https://images.unsplash.com/photo-1540206395-68808572332f?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=562&q=80",
    channelName: "Just Music",
    views: "111tys. wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1567095761054-7a02e69e5c43?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    date: "2 lata temu",
    time: "1:39:18",
  },
  {
    title: "Muzyka Klasyczna do Nauki | Spokojna Muzyka...",
    imgSrc:
      "https://images.unsplash.com/photo-1426604966848-d7adac402bff?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    channelName: "Just Music",
    views: "111tys. wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1567095761054-7a02e69e5c43?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    date: "7 dni temu",
    time: "2:30:21",
  },
  {
    title: "30 Most Beautiful Classical Pieces",
    imgSrc:
      "https://images.unsplash.com/photo-1447752875215-b2761acb3c5d?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    channelName: "JALIDONMUSIC",
    views: "2.1 mln wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1541701494587-cb58502866ab?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    date: "4 miesiące temu",
    time: "1:21:00",
  },
  {
    title: "Music for Relax",
    imgSrc:
      "https://images.unsplash.com/photo-1431794062232-2a99a5431c6c?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    channelName: "OuterSpace",
    views: "56 tys. wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1524169358666-79f22534bc6e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    date: "1 dzień temu",
    time: "49:14",
  },
  {
    title: "Classical Music - Romantic Age",
    imgSrc:
      "https://images.unsplash.com/photo-1470770903676-69b98201ea1c?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    channelName: "Classical Chillout",
    views: "1.1 mln wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1531169509526-f8f1fdaa4a67?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    date: "7 dni temu",
    time: "22:29:13",
  },
  {
    title: "Classical Music - Romantic Age",
    imgSrc:
      "https://images.unsplash.com/photo-1470770903676-69b98201ea1c?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    channelName: "Classical Chillout",
    views: "1.1 mln wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1531169509526-f8f1fdaa4a67?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    date: "7 dni temu",
    time: "22:29:13",
  },
];

export const userYtFeauredData = [
  {
    title: "Classical Music for Reading - Mozart, Chopin...",
    imgSrc:
      "https://images.unsplash.com/photo-1508672019048-805c876b67e2?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1093&q=80",
    channelName: "Just Music",
    views: "111tys. wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1567095761054-7a02e69e5c43?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    date: "2 lata temu",
    time: "1:39:18",
  },
  {
    title: "Muzyka Klasyczna do Nauki | Spokojna Muzyka...",
    imgSrc:
      "https://images.unsplash.com/photo-1530789253388-582c481c54b0?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    channelName: "Just Music",
    views: "111tys. wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1567095761054-7a02e69e5c43?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    date: "7 dni temu",
    time: "2:30:21",
  },
  {
    title: "30 Most Beautiful Classical Pieces",
    imgSrc:
      "https://images.unsplash.com/photo-1476514525535-07fb3b4ae5f1?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    channelName: "JALIDONMUSIC",
    views: "2.1 mln wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1541701494587-cb58502866ab?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    date: "4 miesiące temu",
    time: "1:21:00",
  },
  {
    title: "Music for Relax",
    imgSrc:
      "https://images.unsplash.com/photo-1503220317375-aaad61436b1b?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    channelName: "OuterSpace",
    views: "56 tys. wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1524169358666-79f22534bc6e?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    date: "1 dzień temu",
    time: "49:14",
  },
  {
    title: "Classical Music - Romantic Age",
    imgSrc:
      "https://images.unsplash.com/photo-1517760444937-f6397edcbbcd?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
    channelName: "Classical Chillout",
    views: "1.1 mln wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1531169509526-f8f1fdaa4a67?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    date: "7 dni temu",
    time: "22:29:13",
  },
  {
    title: "Classical Music for Reading - Mozart, Chopin...",
    imgSrc:
      "https://images.unsplash.com/photo-1540206395-68808572332f?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=562&q=80",
    channelName: "Just Music",
    views: "111tys. wyświetleń",
    channelUser:
      "https://images.unsplash.com/photo-1567095761054-7a02e69e5c43?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=634&q=80",
    date: "2 lata temu",
    time: "1:39:18",
  },
];

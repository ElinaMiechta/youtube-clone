import { useRef, useEffect } from "react";
import { Avatar, Button } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import SingleVideo from "../VideoSection/SingleVideo";
import { useDarkMode } from "../utils/darkMode";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import { userYtPopularData } from "../data/videoData";
import { userYtFeauredData } from "../data/videoData";
import Card from "../VideoSection/Card";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import { useMenu } from "../utils/menuContext";

const UserSection = () => {
  const navRef = useRef(null);
  const useSectionRef = useRef(null);
  const { darkMode } = useDarkMode();
  const { wide } = useMenu();

  const animateOnScroll = (htmlElement) => {
    const elem = useSectionRef.current;
    elem.addEventListener("scroll", function () {
      if (elem.scrollTop >= 325) {
        if (wide) {
          htmlElement.classList.add("fixed");
          htmlElement.classList.remove("fixedNarrow");
        } else {
          htmlElement.classList.add("fixedNarrow");
          htmlElement.classList.remove("fixed");
        }
      } else {
        if (wide) {
          htmlElement.classList.remove("fixed");
        } else {
          htmlElement.classList.remove("fixedNarrow");
        }
      }
    });
  };

  useEffect(() => {
    if (useSectionRef.current.scrollTop >= 325) {
      if (wide) {
        navRef.current.classList.add("fixed");
        navRef.current.classList.remove("fixedNarrow");
      } else {
        navRef.current.classList.add("fixedNarrow");
        navRef.current.classList.remove("fixed");
      }
    }
  }, [wide]);

  useEffect(() => {
    animateOnScroll(navRef.current);
  }, [wide]);

  const channelName = localStorage.getItem("user"),
    image = localStorage.getItem("userAvatar");
  return (
    <div
      className="userSection"
      ref={useSectionRef}
      style={{ backgroundColor: `${darkMode ? "#181818" : "#f0f0f0"}` }}>
      <div className="parallax">
        <div className="parallax--bgContainer"></div>
        <div className="parallax--fgContainer">
          <h1>Best of Classical Music</h1>
          <h2>
            Relax. <span>Take your time.</span> Calm.
          </h2>
        </div>
      </div>
      <div className="userSection__header">
        <div className="userSection__header--upper">
          <div className="userSection__header--left">
            <Avatar
              className="userSection__user"
              alt={channelName}
              src={image}
            />
            <div>
              <h3 style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
                {channelName}
              </h3>
              <p>1.65 mln subskrybcji</p>
            </div>
          </div>
          <div className="userSection__header--right">
            <Button>SUBSKRYBUJ</Button>
          </div>
        </div>
        <div
          className="userSection__header--bottom"
          style={{ backgroundColor: `${darkMode ? "#181818" : "#f0f0f0"}` }}
          ref={navRef}>
          <div
            className="userSection__header--navItem underlined"
            style={{
              color: `${darkMode ? "#909090" : "#5F6060"}`,
              borderBottomColor: `${darkMode ? "#909090" : "#181818"}`,
            }}>
            GŁÓWNA
          </div>
          <div
            className="userSection__header--navItem"
            style={{ color: `${darkMode ? "#909090" : "#5F6060"}` }}>
            WIDEO
          </div>
          <div
            className="userSection__header--navItem"
            style={{ color: `${darkMode ? "#909090" : "#5F6060"}` }}>
            PLAYLISTY
          </div>
          <div
            className="userSection__header--navItem"
            style={{ color: `${darkMode ? "#909090" : "#5F6060"}` }}>
            SPOŁECZNOSC
          </div>
          <div
            className="userSection__header--navItem"
            style={{ color: `${darkMode ? "#909090" : "#5F6060"}` }}>
            KANAŁY
          </div>
          <div
            className="userSection__header--navItem"
            style={{ color: `${darkMode ? "#909090" : "#5F6060"}` }}>
            INFORMACJE
          </div>
          <div className="userSection__header--navItem">
            <SearchIcon
              style={{ color: `${darkMode ? "#909090" : "#5F6060"}` }}
            />
          </div>
        </div>
      </div>
      <SingleVideo darkMode={darkMode} />

      <div
        className="videoSection__divider userSectionDivider"
        style={{
          backgroundColor: `${darkMode ? "#333333" : "#cccccc"}`,
          height: "2px",
        }}></div>

      <div className="userSection__ytVideos">
        <div className="userSection__ytVideos--title">
          <h4 style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
            Popularne Filmy
          </h4>
          <PlayArrowIcon
            style={{ color: `${darkMode ? "#909090" : "#5F6060"}` }}
          />
          <span style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
            Odtwórz wszystkie
          </span>
        </div>
        <div
          className="videoSection ytVideos--container"
          style={{ backgroundColor: `${darkMode ? "#181818" : "#f0f0f0"}` }}>
          {userYtPopularData.map((video, index) => (
            <Card
              key={index}
              imgSrc={video.imgSrc}
              title={video.title}
              channelName={video.channelName}
              views={video.views}
              channelUser={video.channelUser}
              date={video.date}
              time={video.time}
              darkMode={darkMode}
            />
          ))}
          <div
            className="ytVideos--container__nextPlay"
            style={{ backgroundColor: `${darkMode ? "#202020" : "#fff"}` }}>
            <NavigateNextIcon
              style={{ color: `${darkMode ? "#5F6060" : "#333333"}` }}
            />
          </div>
        </div>
        <div
          className="videoSection__divider userSectionDivider"
          style={{
            backgroundColor: `${darkMode ? "#333333" : "#cccccc"}`,
            height: "2px",
          }}></div>
        <div className="userSection__ytVideos--title featuredTitle">
          <h4 style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
            Music For Travel
          </h4>
          <PlayArrowIcon
            style={{ color: `${darkMode ? "#909090" : "#5F6060"}` }}
          />
          <span style={{ color: `${darkMode ? "#fff" : "inherit"}` }}>
            Odtwórz wszystkie
          </span>
        </div>
        <div
          className="videoSection ytVideos--container featuredVideos"
          style={{ backgroundColor: `${darkMode ? "#181818" : "#f0f0f0"}` }}>
          {userYtFeauredData.map((video, index) => (
            <Card
              key={index}
              imgSrc={video.imgSrc}
              title={video.title}
              channelName={video.channelName}
              views={video.views}
              channelUser={video.channelUser}
              date={video.date}
              time={video.time}
              darkMode={darkMode}
            />
          ))}
          <div
            className="ytVideos--container__nextPlay"
            style={{ backgroundColor: `${darkMode ? "#202020" : "#fff"}` }}>
            <NavigateNextIcon
              style={{ color: `${darkMode ? "#5F6060" : "#333333"}` }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserSection;

import { createGlobalStyle } from "styled-components";


export const Styles = createGlobalStyle`
.menuContainer__listItem:hover, .appContainer__listItem:hover {
  background-color: ${props => props.darkMode ? '#181818' :'#f0f0f0'}
}
`;
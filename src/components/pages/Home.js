import VideoSection from "../VideoSection";

const Home = () => {
  return (
    <>
      <VideoSection />
    </>
  );
};

export default Home;
